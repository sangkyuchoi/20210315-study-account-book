const dbTran = require('@/util/mysqldb');

module.exports = async ( event ) => {
  return await dbTran( async ( conn ) => {
    const payload = await conn.query( `SELECT id, title, date, sortation, amount
                                       FROM acnt_book.acnt
                                       ORDER BY date DESC`)
    return {
      code: 200,
      message: 'success',
      payload
    }
  })
}