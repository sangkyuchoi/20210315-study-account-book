const dbTran = require( '@/util/mysqldb' )

module.exports =  async  event => {
  const {id, title, date, sortation, amount} = event.body
  return await dbTran( async ( conn ) => {
    const { errno } = await conn.query( 'UPDATE `acnt_book`.acnt SET ? WHERE ID = ?', 
                          [{ title, date, sortation, amount }, id ])
    return { code : !errno ? 200 : 500 }
  })  
};