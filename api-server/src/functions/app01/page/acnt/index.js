const getAcnt = require('./getAcnt')
const deleteAcnt = require('./deleteAcnt')
const updateAcnt = require('./updateAcnt')

module.exports.handler = async ( event, context ) => {
  let method = event.path.method;
  method = method ? method.toLowerCase() : method
  if( method === 'getacnt' ) {
    return await getAcnt( event )
  } else if ( method === 'deleteacnt' ) {
    return await deleteAcnt( event )
  } else if ( method === 'updateacnt') {
    return await updateAcnt( event)
  } else {
    throw new Error( 'Invalid Method' )

  }
}