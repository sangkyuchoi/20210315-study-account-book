const dbTran = require('@/util/mysqldb');

module.exports = async ( event ) => {
  const { acntId } = event.body
  return await dbTran( async ( conn ) => {
    const { errno } = await conn.query( 'DELETE FROM `acnt_book`.acnt where id = ?', [ acntId ])
    return { code: !errno ? 200 : 500}
  })
}