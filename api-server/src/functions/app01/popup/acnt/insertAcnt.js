const dbTran = require( '@/util/mysqldb' )
module.exports =  async  event => {
  const {title, date, sortation, amount} = event.body
  return await dbTran( async ( conn ) => {
    const { errno } = await conn.query( 'INSERT INTO `acnt_book`.acnt ' + 
                          '( title, date, sortation, amount ) VALUES ( ?, ?, ?, ? )', 
                          [ title, date, sortation, amount ] )
    return { code: !errno ? 200 : 500 }
    
  })  
};;