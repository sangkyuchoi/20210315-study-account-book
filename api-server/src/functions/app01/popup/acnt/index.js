const insertAcnt = require('./insertAcnt')

module.exports.handler = async ( event, context ) => {
  let method = event.path.method
  method = method ? method.toLowerCase() : method
  
  if( method === 'insertacnt' ) {
    return await insertAcnt( event )
  } else {
    throw new Error( 'Invalid Method' )
  }
}