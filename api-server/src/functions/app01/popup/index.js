const acnt = require('./acnt')

module.exports.handler = async function( event, context ) {
  let comp = event.path.comp
  comp = comp ? comp.toLowerCase() : comp
  
  if( comp === 'acnt' ) {
    return await acnt.handler( event, context )
  }else {
    throw new Error( 'Invalid Component' )
  }
}
