import Vue from 'vue';
import Router from 'vue-router'

import List from '../page/acntList';
import Add from '../popup/acntAdd';
import Update from '../popup/acntUpdate';
Vue.use(Router);

const routes = [
  {
    path: '/',
    redirect: '/list'
  },
  {
    path : '/list',
    name : 'list',
    component : List,  
  },
  {
    path : '/add',
    name : 'add',
    component : Add,  
  },
  {
    path : '/update',
    name : 'update',
    component : Update,  
  }
]

const router = new Router({
  mode: 'history',
  routes,
})

export default router;