import axios from 'axios'

export default {
  getAcnt(){
    return axios.get( 'http://localhost:9191/app01/page/acnt/getacnt' ).then( res => {
      if( res.data.code !== 200 ) {
         throw res 
      }
      return res
    })
  },
  async deleteAcnt( acntId ){
    try{
      const { data } = await axios.post( 'http://localhost:9191/app01/page/acnt/deleteacnt', acntId )
      return data.code === 200
    }catch( err ){
      console.log( err )
      return false
    }
  },
  async updateAcnt( acnt ) {
    try{
      const { data } = await axios.post('http://localhost:9191/app01/page/acnt/updateacnt', acnt)
      return data.code === 200
    }catch( err ) {
      console.log( err )
      return false
    }
  }
}