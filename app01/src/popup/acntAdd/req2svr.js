import axios from 'axios'

export default {
  async addAcnt( acnt ) {
    try{
      const { data } = await axios.post('http://localhost:9191/app01/popup/acnt/insertacnt', acnt)
      console.log( data );
      if( data.code !== 200 ){
        throw new Error( 'error is response code not 200' )
      }
      return true;
    }catch(err){
      console.error( err );
      return false;
    }
    
  }
}