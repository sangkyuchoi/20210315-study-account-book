import axios from "axios"

export default {
  async updateAcnt( acnt ) {
    try{
      const { data } = await axios.post('http://localhost:9191/app01/popup/acnt/updateacnt', acnt)
      return data.code === 200
    }catch( err ) {
      console.error( err )
      return false
    }
  }
}