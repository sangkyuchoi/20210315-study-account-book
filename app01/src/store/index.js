import _ from 'lodash';
import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex)

const store = {
  state : {
    acntBookList  : [],
  },
  getters : {
    getAcntBookList: state => state.acntBookList,
  },
  mutations : {
    setAllAcntBookList: ( state, acntList ) => {
      state.acntBookList = acntList;
    },
    setAcntBookList: ( state, acnt ) => {
      state.acntBookList.push(acnt)
    },
    updateAcntBookList: ( state, acnt ) => {
      state.acntBookList = _.map(state.acntBookList, o => {
        return acnt.id === o.id ? acnt : o
      })
    },
    deleteAcntBookList: ( state, acntId ) => {
      state.acntBookList = _.remove( state.acntBookList, o => {
        return o.id !== acntId
      })
    }
  },
};

export default new Vuex.Store(store);